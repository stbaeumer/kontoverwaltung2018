 Grundlagen der Objektorientierung
==================================

In den folgenden Kapiteln soll eine Einführung in die **objektorientierte Modellierung** und **Programmierung** anhand eines Kreditinstituts erfolgen. Für die Modellierung finden die Unified Modeling Language (**UML**) und für die Implementierung die Programmiersprache **Javascript** Verwendung. Als einführende Entwicklungsumgebung (IDE = Integrated Development Environment) wird **Microsoft** **Visual Studio Code** herangezogen.

Für den Einstieg in die objektorientierte Programmierung wollen wir im Folgenden ein stark vereinfachtes Kontenverwaltungsprogramm erstellen. Im ersten Ansatz soll es nur möglich sein Konto zu erzeugen und den Inhalt der erzeugten Konten auszugeben. Im weiteren Verlauf des Skripts wird dieses Beispiel weiter ausgebaut.

**Ausgangssituation**

Bei der Sparkasse Kleve werden die folgenden Konten geführt:

![Konten](Pix/Konten.PNG)

Die Geschäftsführung der Sparkasse Kleve hat die Erstellung einer zeitgemäßen Software zur Verwaltung ihrer Kunden und deren Konten in Auftrag gegeben.

1.1 Klasse und Objekt
---------------------

Der Ausgangspunkt der Programmierung in jeder objektorientierten Sprache ist die Überlegung, welche *Objekte der realen Welt* in der Applikation abgebildet werden sollen. In einer Applikation zur Verwaltung von Konten ist zunächst das _Konto_ eine solches Objekt. 

Normalerweise haben solche *Objekte der realen Welt* mehrere interessierende Eigenschaften, die sich oft gut aus Belegen, wie z. B. dem Kontoauszug ablesen lassen.

Im Falle des Kontos sind die Eigenschaften z. B. Kontonummer, Kontoinhaber und Kontosaldo.

Idealerweise werden bei einer Bank sehr viele Konten geführt, die alle **identische Eigenschaften**, aber **unterschiedliche Eigenschaftswerte** aufweisen. Alle Konten haben die Eigenschaft *kontosaldo*, aber die Eigenschaftswerte, also die konkreten Geldbeträge auf jedem Konto sind sehr unterschiedlich.

Sobald der Softwareentwickler zusammen mit dem Auftraggeber die relevanten Objekte mit den interessierenden Eigenschaften identifiziert hat, macht er sich daran einen Bauplan zu erstellen, auf dessen Grundlage **zur Laufzeit** (also wenn das Programm ausgeführt wird) gleichartige Objekte erstellt werden.

> BEISPIEL: In einer Autofabrik werden zur Laufzeit der Produktion zahlreiche Autos auf der Grundlage eines gemeinsamen Bauplans erstellt. Alle Auts haben also die Eigenschaften _Seriennummer_ und _Farbe_. Die Werte der Seriennummern und Farben der Autos (Eigenschaftswerte) sind aber unterschiedlich.

Der Bauplan eines Computerprogramms, auf dessen Grundlage zur Laufzeit des Programms kokrete Objekte erzeugt werden heißt **Klasse**. Die Klasse Konto lässt sich z. B. so skizzieren:

| **Konto** |
| --- |
| kontonummer |
| kontoinhaber |
| saldo |

Ein konkretes Objekt könnte man wie folgt skizzieren. Der Doppelpunkt heißt soviel wie _vom Typ_. Die ganze Kopfzeile liest sich also so: _Kontomüller ist ein Objekt vom Typ Konto_.

>Beispiel: fritzchen ist ein Objekt vom Typ Mensch: ```fritzchen : Mensch```

>Beispiel: batman ist ein Objekt vom Typ Comicheld ```batman : Comicheld``` 

| **kontoMüller : Konto** |
| --- |
| kontonummer = 1234567 |
| kontoinhaber = Müller |
| saldo = 1.000 |

Übersetzt in Javascript-Quelltext sieht die Klasse **Konto** dann so aus:

```Javascript
class Konto{
    this.kontonummer
    this.kontoinhaber
    this.kontosaldo
}
```
Zu erkennen ist ein *Kopf* (```class Konto```), in dem der Bezeichner, also der Name der Klasse steht und ein Rumpf (```{ ... }```), der alles enthält, was im Bauplan stehen soll. Beachten Sie, dass die Klasse ```Konto``` mit einem Großbuchstaben beginnt. Alle Objekte, die von der Klasse erzeugt werden und auch alle Eigenschaften beginnen mit einem Kleinbuchstaben. Um Eigenschaften von sonstigen lokalen Variablen zu unterscheiden, wird ihnen ```this.``` vorangestellt. ```this``` steht für das **dieses konto**, um das es geht.

>Lokale Variablen heißen so, weil sie nur _lokal_ innerhalb der gescheiften Klammern existieren, in der erstmalig erzeugt wurden. Im Gegensatz dazu kann es auch globale Variablen geben, die während der gesamten Laufzeit des Programms leben.

>## Aufgabe 1
> Erstellen Sie einen Unterordner namens ```src```. Das steht für Source, also für Quelltextdateien. In diesem Ordner sollen also alle Dateien mit Javascript-Code aufgenommen werden. Erstellen Sie in einer neuen Datei namens ```konto.js``` im Ordner ```src``` eine Klasse mit dem Bezeichner ```Konto```. Bilden Sie die relevanten Eigenschaften ab 

--------------------------------------------------------

>## Aufgabe 2 
>Identifizieren Sie für für den Fahrradhändler *Velo* und den Autovermieter *Carrent* jeweils ein relevantes Objekt der realen Welt. Implementieren Sie jeweils eine entsprechnde Klasse 

Wie wird nun zur Laufzeit aus dem Bauplan ein Objekt mit konkreten Eigenschaftswerten? Wie wird aus ```Konto``` ein ```konto``` mit der ```kontonummer = 123456```? Wie wird aus ```Comicheld``` ein ```radioactiveman``` mit ```farbe = 'grün'```? Wie wird aus ```Auto```  ```meinAuto``` mit ```leistungInKw = 100.00```?  
Bevor das gezeigt wird, noch kleine Hinweise:

* ```leistungInKw``` ist der Bezeichner einer Eigenschaft. Also wird er kleingeschrieben. Weiterhin besteht er nur aus Buchstaben des Alphabets (also keine Umlaute, Leerzeichen o.ä.). Um dennoch lesbar zu sein, wird hier in *Kamelhöckernotation* geschrieben. Das heißt das Wort beginnt klein und jedes neue Wort wird mit Großbuchstaben begonnen.
* Die Zahl ```100.00``` ist keine Ganzzahl, sondern eine Dezimalzahl. Zur Entwicklungszeit trennen wir Nachkommastellen (wie im Englischen üblich) mit Punkt statt Komma. Zur Laufzeit, also wenn der deutsche Benutzer das Programm bedient, gibt er in einer Textbox natürlich ein Komma ein, weil das in der deutschen Sprache so vorgesehen ist.
* Die Eigenschaftswerte dürfen natürlich auch aus Zeichen außerhalb des Alphabets gebildet werden. Reine Zahlen werden  nicht in Hochkommas gesetzt. Text wird in Hochkommas gesetzt.

Jetzt zurück zur Frage, wie aus dem Bauplan ein konkretes Objekt wird:

Das Erzeugen eines konkreten Objekts passiert im sogenannten *Konstruktor*. Der Konstruktor konstruiert das Objekt. Der Konstruktor ist sozusagen _Bob der Baumeister_, der nach vorgegebenen Regeln das Objekt mit der angegebenen höhe, breite usw. baut. Das soll dabei nicht irgendwie passsieren, sondern genauso, wie der Programmierer das möchte. Die Klasse muss also in einem ersten Schritt ergänzt werden:

```Javascript
class Konto{
    constructor(kontonummer, kontoinhaber, kontosaldo) {
        this.kontonummer = kontonummer
        this.kontoinhaber = kontoinhaber
        this.kontosaldo = kontosaldo
    }    
}
```

Der Konstruktor hat einen Kopf und einen Rumpf. Im Kopf steht hinter dem reservierten Wort ```constructor``` in runden Klammern eine Anzahl von sogenannten Parametern. Hier sind es 3 Parameter, die der Konstruktor erwartet, um im Rumpf tätig werden zu können. 

>Parameter sind wie die Zutaten für ein Backrezept. Wenn sämtliche Parameter in den runden Klammern in der richtgen Reihenfolge an den Constructor übergeben werden, dann backt der Construktor in seinem Rumpf alles wie gewünscht zu einem kuchen-Objekt zusammen.  

Die Verarbeitung im Rumpf des obigen Konto-Klasse-Konstruktors ist wenig aufwendig. Es werden lediglich alle entgegengenommenen Zutatenwerte zugewiesen an die gleichnamige Eigenschaft der Klasse. Danach ist das konto-Objekt fertiggebacken. 

> Zur Verdeutlichung: Die Anweisung ```this.kontonummer = kontonummer``` liest man von rechts nach links wie folgt: _Der Wert der lokalen Variablen mit dem Bezeichner ```kontonummer``` wird zugewiesen ```=``` an die Eigenschaft ```this.kontonummer```_

>## Aufgabe 3 
>Bauen Sie Konstruktoren in alle Klassen Ihrer Projekte!

Nun ist alles vorbereitet. Was noch fehlt ist der sogenannte Konstruktoraufruf mit der Übergabe konkreter Parameterwerte. Der geschieht nun aus einer neu zu erstellenen Datei namens ```main.js``` Die Datei liegt ebenfalls im Ordner ```src```.

```Javascript
let konto  
konto = new Konto(4711, 'Müller', 100.00)
```

Die Anweisung ```let konto``` ist die sogenannte **Deklaration**. Also die Bekanntmachung, dass es ein neues Konto namens *konto*
geben soll. Die zweite Anweisung ist zunächst die **Instanziierung**. Eine Instanziierung erkennt man stets am reservierten Wort **new**. Bei der Instanziierung werden konkrete Speicherzellen des Arebitsspeichers des Computers freigeräumt und mit dem konto-Objekt verknüpft. Anschließend werden die Speicherzellen mit den konkreten Werten ```(4711, 'Müller', 100.00)``` beschrieben. Man sagt, dass Objekt wird ```initialisiert```.

>Der Dreischritt aus **Deklaration, Instanziierung und Initialisierung** wird immer, immer wieder von Bedeutung sein! 

Um später das Konto anzeigen zu können, muss in die ```main.js``` eine ```function``` namens ```ausgeben``` eingebaut werden. Das Verb deutet darauf hin, dass Funktionen stets etwas machen. Man erkennt Funktionen an den runden Klammern hinter dem Bezeichner. Innerhalb des Rumpfes der Funktion werden Anweisungen formuliert, die Zeile für Zeile abgearbeitet werden. In den folgenden Codezeilen werden die Werte von Kontonummer, Kontoinhaber usw. zugewiesen an die Eigenschaft ```ìnnerHTML``` desjenigen Elements, das die ID ```ausgabe``` hat.

```Javascript
function ausgeben(kto) {
    document.getElementById("ausgabe").innerHTML = konto.kontonummer + "<br>" + konto.kontoinhaber + "<br> ..."
}
``` 


>## Aufgabe 4 
>Deklarieren, Instanziieren und Initialisieren Sie konkrete Objekte aus den Klasen Ihrer Projekte.!

1.2 Die Grafische Oberfläche
----------------------------

Die grafische Oberfläche des Programms ist von der Logik des Programms zu trennen. Das heißt, dass innerhalb des Projektordners unterschiedliche Dateien jeweils die 
Logik oder die Darstellung der Oberfläche abbilden. Die Oberfläche mit allen Textfeldern, Buttons usw. wird in einer Datei namens *index.html* gestaltet.

Die Datei muss den Namen *index* tragen, damit beim Aufruf einer Webseite die richtige Startseite angezeigt wird. Ohne diese Vereinbarung müsste der Anwender im Falle der Homepage des BKB beispielsweise _berufskolleg-borken.de/index.php_ aufrufen. Das ist natürlich nicht im Sinne der Anwender, funktioniert aber durchaus auch. Probieren Sie das aus!

Die Datei namens *index* liegt auf der obersten Ebene innerhalb des Projektordners. Also liegt sie neben dem *src*-Ordner.

1.2.1 HTML
----------
HTML steht für Hypertext Markup Language. Es handelt sich also um eine Auszeichnungssprache. 
Das wiederum bedeutet, dass man mit HTML festlegen kann, 

* welcher Text im Browser angezeigt wird, 
* welche Bilder an welcher Stelle angezeigt werden, 
* welche Buttons wo erscheinen, 
* welche Checkboxen usw. an welcher Stelle angezeigt werden. 

Wer auf interaktive Elemente wie Textboxen, Checkboxen, Labels, die auf Knopfdruck etwas bestimmtes anzeigen sollen usw., verzichten kann, der kann mit HTML bereits vollständige 
Webseiten erstellen. Solche reinen HTML-Webseiten sind aber heute sehr selten. Sobald dynamische Inhalte hinzukommen, muss HTML mit einer Programmiersprache kombinert werden. Das gilt selbst für so kleine dynamische Inhalte wie ein Menü, das bei *Mouseover* aufklappt.

Mit der Programmiersprache ist es dann natürlich auch möglich statische Elemente wie die Aufschrift auf einem Label dynamisch zu verändern.

1.2.2 Aufbau einer HTML-Datei
-----------------------------
Typisch für HTML sind die *Tags*, die meist paarweise auftreten und irgendetwas umschließen.
Beispielsweise umschließen die Tags ```<html> ... </html>``` die gesamte HTML-Seite. Lediglich die Angabe des *Doctypes* steht oberhalb dieser Tags. 

Beachten Sie die Schreibweise:

* Tags stehen in spitzen Klammern
* Das erste Zeichen innerhalb der spitzen Klammern des schließenden Tags ist ein Slash **/**.

>KOMMENTAR: EIn Kommentar wird in HTML so geschrieben: ```<!--hier kommt der Kommentar hin-->```
>In Kommentaren erläutert der Programmierer seinen Quelltext. Alles, was in einem Kommentar steht, wird vom Browser ignoriert und
nicht angezeigt.

Alles, was im Browserfenster tatsächlich angezeigt werden soll, steht innerhalb der ```<Body>```-Tags.

Der ```<title>Kontoführung</title>``` steht innerhalb des ```<head> ... </head>``` und wird oberhalb des Browserfensters angezeigt.
Innerhalb des ```<body> ... </body>``` könnte es zunächst mit einer Überschrift losgehen: ```<h4>Kontoführung</h4>```. H4 steht für _Heading_ der Größe 4.

Eine Spielswiese zum Ausprobieren von HTML finden Sie hier: [https://www.w3schools.com/html/](https://www.w3schools.com/html/).

Die folgenden Codezeilen stehen unterhalb der Überschrift und werden im Folgenden besprochen:

```HTML
<div>
            <label for="kontonummer"> Kontonummer: </label>
            <input type="text" name="kontonummer" id="ktonr" placeholder="50607080" value="50607080">
</div>  
```

* ```<div>``` steht für einen Bereich. So ein Bereich dient der Strukturierung des HTML-Quelltextes. Der Bereich kann auch mit einer ID versehen werden, so dass er über die ID beispielsweise sichtbar oder unsichtbar geschaltet werden kann. Weiterhin kann für einen Bereich ein bestimmtes Aussehen (z. B. grüner Hintergrund) festgelegt werden.
* ```<label>``` steht für einen Bereich in dem z. B. Text angezeigt wird. Das Label im gezeigten Quelltext dient als Beschriftung der darunterliegenden Textbox. 
* ```<input type="text" ... >``` steht für eine Textbox, in die der Anwender des Programms Text eintragen soll. Da auf dem Label darüber 
'Kontonummer' steht, liegt es nahe, hier die Kontonummer einzutragen. Damit die Programmiersprache den Wert der Textbox verarbeiten kann, muss sie unter einer eindeutigen ```ID``` erreichbar sein.
* ```placeholder``` dient dem Anwender des Programms als Hilfe bei der Eingabe.
* ```value``` ist der Standardwert, der an die Programmiersprache weitergegeben wird, wenn die Textbox leerbleibt.

```HTML
<br>
<button id="btnKontoErzeugen">Konto erzeugen</button>
```
Unterhalb des Formulars steht ein Button, also eine Schaltfläche, die bei Klick etwas auslöst. Sobald er gedrückt wird, startet die Verarbeitung. Die Werte aus der Textbox werden eingelesen und verarbeitet. Zuletzt wird das Ergebnis ausgegeben. Diesen Dreischritt nennt man auch _EVA_, also Eingabe, Verarbeitung und Ausgabe. ```<br>``` steht für einen Zeilenümbruch (_break_).

```HTML
<div id="ausgabe"></div>
```
Auf dem ```ausgabe```-Bereich wird die Ausgabe angezeigt.


>## Aufgabe 5 
>Erstellen Sie eine *index.html*-Datei im Projektordner von *Konto* und *Carrent*.
>In der Datei entwerfen Sie in HTML eine Grundstruktur mit einem Label im Body.

1.3 Verknüpfung von Logik und Oberfläche
----------------------------------------

Damit zur Laufzeit Eingaben auf der grafischen Oberfläche (```index.html```) von der Logik verarbeitet werden könnnen, um zuletzt wieder auf die Oberfläche zu schreiben, müssen sowohl die ```index.html``` als auch die ```main.js``` angepasst werden:

```HTML
  </body>
  <script src="src/konto.js"></script>
  <script src="src/main.js"></script>
</html>
```
Unterhalb des Body, aber noch innerhalb der ```<HTML>```-Tags werden in der ```index.html``` die beteiligten Javascript-Dateien verknüpft, indem der Pfad zur JS-Datei angebenen wird.

Weiterhin muss ein ```Eventlistener``` in der ```main.js``` implementiert werden. Dessen Aufgabe ist es, beim ```click``` auf den Button ```btnKontoErzeugen``` die Funktion ```ausgeben``` aufzurufen:

```Javascript
  document.getElementById("btnKontoErzeugen").addEventListener("click", ausgeben)
```

> Vereinabrung für die Bennennung: Wenn auf dem Button 'Konto erzeugen' steht, dann sollte die ID ```btnKontoErzeugen``` heißen. Der Eventlistener sollte dann eine Funktion namens kontoErzeugen aufrufen. Entsprechend sollten Textboxen das Präfix 'tbx' in der ID bekommen. Weiterhin: Lables: 'lbl', Comboboxen: 'cbx'

Im Folgenden wird die bisherige ```Main.js``` als Ganzes gezeigt und besprochen:

```Javascript
1. function kontoErzeugen() {    
2.    const kontonummer = document.getElementById("tbxKontonummer").value
3.    const kontoinhaber = document.getElementById("tbxKontoinhaber").value
4.    const kontosaldo = parseFloat(document.getElementById("tbxKontosaldo").value)

5.    let konto = new Konto(kontonummer,kontoinhaber,kontosaldo)

6.    ausgeben(konto)
7. }
8.
9. function ausgeben(konto) {    
10.    document.getElementById("ausgabe").innerHTML = konto.kontonummer + "<br>" + konto.kontoinhaber + "<br>" + konto.kontosaldo
11.}
12.
13. document.getElementById("btnKontoErzeugen").addEventListener("click", kontoErzeugen)
```

| Zeile | Beschreibung |
| --- | --- |
|1 | Eine Funktion namens ```kontoErzeugen()``` wird erstellt. Bitte klein beginnen im Sinne der Kamelhöcker-Notation.|
| 2 | Der Wert der Eigenschaft ```value``` des Elements mit der ```ID = "tbxKontonummer"``` auf der ```index.html```-Seite wird zugewiesen an die Konstante namens ```kontonummer```.|
| 5 | Deklaration, Instanziierung und Initialisierung eines Objekts vom Typ Konto.|
| 6 | Das Kontoobjekt wird als Parameter an die Funktion ```"ausgeben"``` übergeben|
| 10 | Der Ausgabestring wird zugewiesen an die Eigenschaft ```innerHTML``` des Elements mit der ```ID = "ausgabe"```|


1.4 Typische Fehler und deren Behandlung
----------------------------------------

Das Auffinden und korrigieren von Fehlern beginnt im Browser durch den Tastendruck auf die Funktionstaste ```F12```. Es öffnet sich die Entwicklerkonsole. Im Chrome werden Fehler oben rechts angezeigt:

![Fehlermeldung](Pix/fehlermeldung.PNG)


Klicken Sie auf das rote Icon mit dem weißen X.

**Beispiel 1**

![Null](Pix/null.PNG)

Die Fehlermeldung besagt, dass irgendetwas mit der Eigenschaft ```'value'``` ```null```  ist. An der angegebenen Stelle in der main.js sieht es so aus:

![NullValue](Pix/nullvalue.PNG)

Vermutlich stimmt die ```tbxKontonummer``` nicht mit der ID in der ```index.html``` überein.

**Beispiel 2**

![NotDefined](Pix/NotDefined.PNG)

Die Fehlermeldung besagt, dass ein Objekt namens konto```not defined```, also nicht definiert ist. An der angegebenen Stelle in der main.js sieht es so aus:

![NotDefinedMain](Pix/NotDefinedMain.PNG)

Vermutlich ist das Konto in Zeile 10 der main.js nicht instanziiert worden. Es fehlt die Anweisung ```let konto = new Konto(...)```.


Vererbung in Javascript
-----------------------
...



Fehlerbehandlung in Javascript
------------------------------
Sytanxfehler, die während der Entwicklungszeit auftreten, werden vom Compiler gemeldet und anschließend vom Programmierer korrigiert. Erst wenn alles nach Vorgabe ausprogrammiert ist und kein Compilerfehler das Starten des Programms verhindert,  wird das Programm an den Kunden ausgeliefert.

**Warum stürzen Programme dennoch ab?**

Auch zur Laufzeit des Programms kann es zu Fehlern kommen, die behandlungsbedürftig sind. Soll heißen, dass der Programmierer schon beim Programmieren vorhersehen muss, zu welchen Problemen es beim Kunden möglicherweise kommen kann. 

*Beispiele:*

* Der Anwender gibt eine 0 in eine Textbox ein. Im Laufe des Programms wird aber durch die Zahl dividiert.
* Der Anwender soll eine Zahl eintippen, tippt aber Buchstaben
* Der Anwender versucht einen negativen Betrag von einem Konto auszuzahlen, wodurch es zu einer Einzahlung käme
* Das Programm kann die Verbindung zur Datenbank nicht öffnen
* Das Programm kann eine Datei nicht öffnen, weil sie schon geöffnet ist oder nicht existiert
* Der Anwender tippt eine falsche IBAN ein

Wenn es zur Laufzeit dann beim Anwender tatsächlich zu einem Fehler kommt, muss eine möglichst aussagekräftige Meldung dem Anwender mitteilen, was er falsch gemacht hat.

**So wird's gemacht**

Dort, wo es im Quelltext zu Fehlern kommen kann, muss der Quelltext in den Rumpf einer ```try```-Kontrollstruktur eingebettet werden. Das ist beispielsweise beim Einlesen von Textboxen der Fall. 

>Jeder logische Ausdruck ist entweder ```true``` oder ```false```. Die folgenden Werte werden alle als ```false``` erkannt: 
> ```false, 0, -0, null, undefined, NaN, ""``` 
>Alle anderen Werte werden als ```true``` erkannt.
Das Ausrufezeichen vor einem logischen Ausdruck kehrt den Wert von ```true``` nach ```false``` und umgekehrt.

Mit ```throw``` legt der Programmierer genau diejenige Stelle im Quelltext fest, an der ein Fehler die von ihm definierte Meldung werfen soll. 

```Javascript
// Versuche ...
try {    
      // ... die Anweisungen hier im Rumpf auszuführen.

    if(x < 0){
      // Wenn eine negative Zahl eingegeben wird, wird ein Fehler geworfen:
      throw new Error("Es können keine negativen Werte vom Konto abgehoben werden!")
    }

    if(isNaN(x)){
      // Wenn der Wert von x keine Zahl ist, wird der Fehler geworfen:
      throw new Error("Fehler! Es dürfen nur Zahlen eingegeben werden! Sie haben eingegeben: " + x)
    }    

    if (!anzahlDerPersonen){
      // Wenn die Anzahl der Personen 0 ist, kann der Preis je Person nicht ausgerechnet werden:    
      throw new Error("Die Anzahl der Personen darf nicht 0 sein.")    
    }
} 
```

Sobald ein Fehler geworfen wird, wird die Abarbeitung der Anweisungen im ```try```-Block abgebrochen und im ```catch```-Block fortgesetzt. Der ```catch```-Block muss also zwingend auf den ```try```-Block folgen. Im ```catch```-Block kann dann beispielsweise ein PopUp-Fenster (```alert('...')```) gestartet werden, das die Meldung an den Anwender ausgibt.

```Javascript
try {...}    
catch (error){
  alert(e.Name + ":" + error.message)
  console.log(error.name + ":" + error.message)
}
```

Beachten Sie, dass ```error``` selbst auch ein Objekt ist, das verschiedene Eigenschaften hat, die jeweils ausgelesen werden können: 

```Javascript
  catch(error) {
    console.log(error.name) // Error
    console.log(error.message) // "Fehler! Es dürfen nur ..."
}
```

Optional kann unterhalb von ```catch``` noch ein ```finally```-Block eingebaut werden. Die Anweisungen im ```finally```-Block werden immer ausgeführt, egal ob ein Fehler geworfen wurde oder nicht. Sinnvoll ist der Einsatz immer dort, wo etwas geöffnet wurde, was wieder geschlossen werden sollte. Dazu gehören Datenbankverbindungen oder Dateien.

```Javascript
try { ... }    
catch (error){ ... }
finally{
  console.Log("Verarbeitung beendet oder abgebrochen.")
}
```

**Wie können falsche Benutzereingaben bereits bei der Eingabe behandelt werden?**

Je nach Sachverhalt kann man das Exeption-handling dem Browser überlassen, ohne eine Zeile Javascript programmieren zu müssen. Das folgende Beispiel zeigt wie es geht:

```HTML

<input id="demo" type="number" min="5" max="10" step="1" ...

```
Erwünscht ist hier offensichtlich die Eingabe einer Ganzzahl zwischen 5 und 10.


**Aufgabe:**
Behandeln Sie mögliche Ausnahmen in Ihrer Kontoverwaltung!


### Native HTML-Prüfungen nutzen - ohne Javascript

Strategie: Je früher ein Eingabefehler bemerkt wird, desto besser.

Manches lässt sich direkt mit HTML5-Mitteln abfangen.

(Ausführlicher Beitrag: s. https://css-tricks.com/form-validation-part-1-constraint-validation-html/)

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<title>FirstBlog</title>
	</head>
	<body>

		<h4>Datenvalidierung mit HTML5</h4>
		<form action="">
			<ul class="form-fields">
				<li>
					<label for="Nachname">Nachname: </label>
					<input type="text" id="tbxNachname" required minlength="3">
				</li>
				<li>
					<label for="alter">Alter:</label>
					<input type="text" id="tbxAlter" pattern="[0-9]" maxlength="2">
				</li>
				<li>
					<input type="submit" class="button" value="Submit">

				</li>
			</ul>
		</form>
	</body>
</html>
```

| Vorteile                                 | Nachteile                                |
| ---------------------------------------- | ---------------------------------------- |
| Die Daten werden nur weiter behandelt, wenn alle die Vorprüfung passieren | Die Rückmeldungen sind sehr allgemein, keine spezifische Rückmeldung, welches Feld betroffen (Fehlermeldung kannn icht angepasst werden) |
| Kein unnötiger Datentransfer             | Fehler kann nicht im Kontext mit anderen eingegebenen Daten geprüft werden. |
| Kein Programmieraufwand                  | Der User erhält die Rückmeldung erst nach dem Ausfüllen aller Daten und Click auf den submit-Button |
| Reguläre Ausdrücke können auch komplexe Baustrukturen prüfen, wie Telefonnummern oder Email-Adressen | Reguläre Ausdrücke sind nicht leicht zu lesen. Allerdings sind Standardprüfungen sehr kurz und leicht möglich. |
|                                          | Verhält sich nicht in allen Browsern gleich. |

#### Validierung mit Javascript

Studien zur Usability von Formularen haben gezeigt, dass  wenn der Fehler direkt beim Schreiben bzw. beim Verlasen des jeweiligen Feldes angezeigt wird, und dieser Fehler sichtbar bleibt, bis er behoben ist, dieses als sehr anwenderfreundlich erfahren wird (vgl. Studie von Holst und Wroblewski).

Daher macht es Sinn, die Fehlerbehandlung in Javascript anzugehen und zwar immer dann, wenn ein Eingabefeld verlassen wird, also wenn irgendwo außerhalb des Eingabefelds geklickt wird. Dieses Event heißt "onblur" und steht für Inputs, Buttons, Selects und Textareas zur Verfügung. 

Anfügen des Eventhandlers (Achtung! Hier **blur** anstelle von **onblur**!):

```js
document.getElementById("ktonr").addEventListener("blur", eingabeZiffernPruefen)
```

Will man den Anwender auch "zwingen" den Eingabefehler zu beheben, so kann man den Fokus solange beibehalten, bis der Fehler ausgeräumt ist. Außerdem ist es chick, wenn die "Baustelle" typographisch hervorgehoben wird (Roter Rahmen, rote Schrift, anderer Hintergrund, ..). 

Hierfür ist es sinnvoll im css eine Klasse "Fehler" mit der gewünschten Hervorhebung zu gestalten.

```css
error {
        color: red;
        border-color: red;
     
}
```

Diese Klasse wird dem Eingabefeld immer dann zugewiesen, wenn beim Verlassen ein Fehler festgestellt wird und wieder entfernt, wenn der Fehler korrigiert wurde. Für einen Spezifischen Fehlerhinweis, sollte man noch ein Label für einen Fehlerhinweis ergänzen.

#### Beispiel Methode eingabeZiffernPruefen()

```js
function eingabeZahlPruefen(){
   var inpKto = document.getElementById("ktonr")
   var errMessage = document.getElementById("errKontonummer")
    if(isNaN(inpKto.value)){
       //
       inpKto.classList.add("error")
       errMessage.innerHTML="Für eine Kontonummer nur Ziffern eingeben!"
       document.getElementById("btnKontoErzeugen").removeEventListener("click",kontoErzeugen)
      // Hier wird die Verknüpfung des Buttons mit der Erzeugenmethode gelöst, da im Fehlerfall diese kein gültiges Konto liefern würde.

       inpKto.focus()
        
    }else{
       inpKto.classList.remove("error")
       errMessage.innerHTML=""
       document.getElementById("btnKontoErzeugen").addEventListener("click",kontoErzeugen)
    }    
}
```

##### Aufgabe

1. Ergänzen Sie ihr Programm um diese Prüfung und testen Sie diese. Notieren Sie zunächst für jede "Baustelle" css-Datei, Index-Datei und Main-Programm welche Änderungen Sie wo einbauen möchten.

2. Schreiben Sie zu allen Programmzeilen einen Kommentar (Kommentare sind Texte die der Compiler überliest, die also nicht ausgeführt werden, den  Programmierenden aber hilft Übersicht zu bewahren. 

   ```js
   // Dies ist eine Zeilen Kommentar.

   /* und dies ein
   Blockkommentar 
   übermehrere Zeilen */
   ```

3. Ergänzen Sie nun zu allen Eingabefeldern passende Eingabeprüfungen nach diesem Muster.

4. (Zusatzaufgabe) 
   In Deutschland dürfen Kontonummern für den Zahlungsverkehr nur aus Ziffern und maximal aus zehn Stellen bestehen, das wurde im DFÜ-Abkommen so festgelegt.  Solche Prüfungen lassen sich sehr gut mit sogenannten regulären Ausdrücken prüfen.

   Bauen Sie die obige Regel in die Überprüfung der Kontonummer ein. Sie können dabei reguläre Ausdrücke nutzen oder es auch mit bekannten Verfahren lösen.

   ##### Beispiel

   Alle aktuellen Browser kennen den Befehl match(). Dieser überprüft anhand von regulären Ausdrücken (Regular-Expressions) einen Text und gibt entweder null (wenn er nicht passt) oder den Text (wenn er passt) zurück.

   Ein Beispiel ist die Überprüfung von Postleitzahlen. Diese bestehen in Deutschland aus 5 Ziffern, auch die erste Ziffer kann eine 0 sein (z. B. gehört zu Leipzig u.a. die PLZ  *04103*).

   ```js
   var text ="12345";
   alert(text.match(/^[0-9]{5}$/));
   ```

   Das Beispiel gibt nur bei exakt 5 Ziffern einen String aus.

   In Deutschland dürfen Kontonummern für den Zahlungsverkehr maximal aus zehn Stellen bestehen, das wurde im DFÜ-Abkommen so festgelegt. 

   ​



# Datenbanken machen Daten dauerhaft verfügbar

Bisher waren stets alle eingegebenen Daten nach dem Laden des Programms gelöscht. Allerdings ist es in der Realität meistens so, dass einmal erfasste Daten dauerhaft zur Verfügung stehen sollen. 
Hier kommen die Datenbanken ins Spiel. Verschiedene Hersteller bieten solche Datenbanken als installierbares Programm oder als Dienst über das Internet an. Das Gute dabei ist, dass trotz unterschiedlicher Produkte die allermeisten Datenbanken dieselbe Sprache sprechen. Diese gemeinsame Sprache zum Erstellen (```CREATE```), Auslesen (```READ```), Ändern (```UPDATE```) und Löschen (```DELETE```) von Daten in Datenbanken heißt **SQL**. Das steht für Structured Query Language.

## Wie sage ich’s meiner Datenbank? 

Wer mit seiner Datenbank kommunizieren möchte, muss also SQL sprechen. SQL-Befehle beginnen stets mit einem englischen Verb im Imperativ. Grundsätzlich lassen sich SQL-Befehle wie ein vollständiger, englischer Satz von links nach rechts lesen: 

* ```CREATE TABLE meineTabelle (id INTEGER);``` 
* ```INSERT INTO meineTabelle (id) VALUES (1);``` 
* ```SELECT id FROM meineTabelle;``` 

Beachten Sie dabei Folgendes:

* Reservierte SQL-Worte wollen wir großschreiben
* Anweisungen sollten mit einem Semikolon enden

Ein guter Ausgangspunkt für das Erlernen von SQL ist die Webseite DB-Fiddle [https://www.db-fiddle.com/], die wie folgt daherkommt:

![DB-Fiddle](Pix/DB-Fiddle.PNG)

Die drei Bereiche unterhalb der blauen Menü- & Befehlsleiste sind vorgesehen für die Manipulation des **Schemas** (```CREATE, INSERT, UPDATE usw.```), die **Abfrage** (```SELECT```) und das **Ergebnis** der Abfrage. Das Ergebnis ist dann stets wieder *eine* Tabelle, die die gewünschten Spalten und Zeilen einer oder mehreren Tabellen der Datenbank zurückgibt.

Ohne auch nur ein Zeichen tippen zu müssen, können Sie das im Bild gezeigte Beispiel laden (**Load Example**) und die Abfrage ausführen (**Run**). Wählen Sie (wie in der Abbildung gezeigt) SQLite in der Version 3.18 Beginnen Sie mit dem Aufrufen des Beispiels! Fügen Sie doch einmal einen dritten Datensatz ein!

## Aufgabe

Erstellen Sie das Datenbankschema für den Sachverhalt aus der Hausaufgabe (s. Abbildung). Es empfiehlt sich nicht alles auf einmal einzutippen, sondern tabellenweise vorzugehen und regelmäßig die Syntax überprüfen zu lassen. 

Konsultieren Sie das Internet, um zu erfahren, wie beispielsweie mehrere Spalte in einer Tabelle eingetragen werden.

![ERM-Projekte](Pix/ERM-Projekte.PNG)

### Lösungshinweis:

Der Schalter ```PRAGMA foreign_keys = 1;``` ist SQLite-spezifisch und erzwingt **Referentielle Integrität**.
Das bewirkt in dem konkreten Kontext beispielsweis, dass 
 * keine Abteilung gelöscht werden kann, in der noch Mitarbeiter existieren.
 * einem Mitarbeiter keine Abteilung zugewiesen werden kann, die nicht existiert. 

```SQL

PRAGMA foreign_keys = 1;

CREATE TABLE Mitarbeiter (
  MitarbeiterId INTEGER,
  MitarbNName VARCHAR(30),
  MitarbVName VARCHAR(30),
   AbteilungId INTEGER,  
  PRIMARY KEY(MitarbeiterId),
  FOREIGN KEY (AbteilungId) REFERENCES Abteilung(AbteilungId)
);

CREATE TABLE Abteilung (
  AbteilungId INTEGER,
  Bezeichnung VARCHAR(30),
  PRIMARY KEY(AbteilungId)
);

CREATE TABLE Projekt (
  ProjektId INTEGER,
  Projektbeschreibung VARCHAR(30),
  PRIMARY KEY(ProjektId)
);

CREATE TABLE MitarbeiterProjekt (
  MitarbeiterId INTEGER,
  ProjektId INTEGER,
  Arbeitsstunden INTEGER,
  PRIMARY KEY(MitarbeiterId,ProjektId),
  FOREIGN KEY (MitarbeiterId) REFERENCES Mitarbeiter(MitarbeiterId),
  FOREIGN KEY (ProjektId) REFERENCES Projekt(ProjektId)
);

INSERT INTO Abteilung (AbteilungId, Bezeichnung) VALUES (1, 'Einkauf');
INSERT INTO Abteilung (AbteilungId, Bezeichnung) VALUES (2, 'Controlling');
INSERT INTO Abteilung (AbteilungId, Bezeichnung) VALUES (3, 'Verkauf');
INSERT INTO Projekt (ProjektId, Projektbeschreibung) VALUES (1, 'Ein Projekt');
INSERT INTO Mitarbeiter (MitarbeiterId, MitarbNName, MitarbVName, AbteilungId) VALUES (1, 'Müller', 'Egon', 1);
INSERT INTO Mitarbeiter (MitarbeiterId, MitarbNName, MitarbVName, AbteilungId) VALUES (2, 'Meier', 'Jutta', 1);

INSERT INTO MitarbeiterProjekt (MitarbeiterId, ProjektId, Arbeitsstunden) VALUES (1,1, 10);

-- Keine Verletzung der Referentiellen Integrität:

DELETE FROM Abteilung WHERE AbteilungId = 3;

-- Verletzung der Referentiellen Integrität (FOREIGN KEY constraint failed):

DELETE FROM Abteilung WHERE AbteilungId = 1;
INSERT INTO Mitarbeiter (MitarbeiterId, MitarbNName, MitarbVName, AbteilungId) VALUES (1, 'Müller', 'Egon', 7);
INSERT INTO MitarbeiterProjekt (MitarbeiterId, ProjektId, Arbeitsstunden) VALUES (1,6, 10); 


```










#### Anhang (Reguläre Ausdrücke) 

* übernommen aus: http://blog.weblogie.de/webentwicklung/merkblatt-regular-expressions/

## Die Syntax der Regular Expressions

### Wichtige Regeln

Eine RegEx muss am Anfang und am Ende identische Zeichen enthalten. Dies kann jedes Zeichen sein außer den alphanumerischen Zeichen und dem Backslash (), da dieses für das escapen verwendet wird.

**Standardzeichen sind / oder %**

### Vordefinierte Zeichenklassen

| [a-z]                | Alle Kleinbuchstaben                     |
| -------------------- | ---------------------------------------- |
| [A-Z]                | Alle Großbuchstaben                      |
| [0-9] oder \d        | Alle Ziffern                             |
| [a-zA-Z0-9_] oder \w | Alle Buchstaben, Ziffern und der Unterstrich |
| [\r\n\t\f] oder \s   | Wagenrücklauf, New Line, Tabulator, Seitenwechsel (Steuerzeichen) |
| . (Punkt)            | Alle Zeichen außer n                     |
| \b                   | Wortgrenze d.h. ein Tab oder ein Leerzeichen o.Ä. |

### Operatoren

| ^     | Wenn als erstes Zeichen der RegEx eingesetzt, dann muss der zu überprüfende String mit der Regel beginnen. |
| ----- | ---------------------------------------- |
| $     | Wenn als letztes Zeichen der RegEx eingesetzt, dann muss der zu überprüfende String mit der Regel enden. |
| []    | Einer der in den Klammern enthaltenen Buchstaben muss an dieser Stelle stehen. |
| ()    | Gruppiert und selektiert: Alle in den Klammern enthaltenen Buchstaben müssen in der angegebenen Reihenfolge vorkommen. Die Selektion kann anschließend als Gruppierung behandelt werden. Die jeweiligen Selektionen können im Anschluss ausgegeben werden.Beispiel: Erste Klammer kann per $1 ausgegeben werden (2. Klammer über $2 usw…) |
| (?: ) | Gruppiert (siehe oben) jedoch ohne Selektion: Die Inhalte sind nicht mehr im Anschluss über die Variablen zugreifbar. |
| (?= ) | Ausdruck: “Muss gefolgt werden von” Die Zeichenkette vor der Klammer muss gefolgt werden von der Zeichenkette in der Klammer. Die Zeichenkette in der Klammer wird aber nicht in das Ergebnis geschrieben. |
| (?! ) | Ausdruck: “Darf nicht gefolgt werden von” Die Zeichenkette vor der Klammer darf nicht von der Zeichenkette in der Klammer gefolgt werden. Die Zeichenkette in der Klammer wird nicht in das Ergebnis geschrieben. |

### Inversion von Zeichenklassen

\d = nur Ziffern
\D = alle Zeichen bis auf Ziffern
Oder mit Zirkumflex: [^0-9]

### Zeichen die mit (Backslash) escaped werden müssen

( ) { } [ ] \ | / + ? * . ^ & -

### Anzahl der Zeichen festlegen

| x+     | Das Zeichen x muss min. einmal vorkommen. Kann beliebig oft vorhanden sein => x{1,} |
| ------ | ---------------------------------------- |
| x*     | Das Zeichen x kann beliebig oft vorkommen, also auch gar nicht => x{0,} |
| x?     | Das Zeichen x kann, muss aber nicht auftreten => x{0,1} |
| x{m}   | Das Zeichen x muss genau m mal auftreten |
| x{m,}  | Das Zeichen x muss min. m mal auftreten. Kann unendlich oft vorhanden sein. |
| x{,n}  | Das Zeichen x darf höchstens n mal auftreten. |
| x{m,n} | Das Zeichen x zwischen m und n mal auftreten. |

### Die Modifier

werden hinter die Regular Expression geschrieben. ( Bsp: /^Test$/i )

| i    | Mit diesem Modifier wird nicht mehr auf die Groß/Kleinschreibung geachtet. |
| ---- | ---------------------------------------- |
| g    | Mit diesem Modifier werden alle Matches geliefert und nicht nur das erste |
| m    | Mit diesem Modifier werden Zeilenbrüche als Start und Ende für die Operatoren ^ und $ gewertet.Bsp: /.$/mHier muss am Ende jeder Zeile ein . (Punkt) vorhanden sein. |
| s    | Dieser Modifier bewirkt, dass alle Zeichen (auch Zeilenumbrüche) dem . (Punkt) entsprechen. |

## Beispiele

| /^test\d?/i                   | Die zu überprüfende Zeichenkette darf nur mit test bzw. test1 – test9 beginnen, wobei nicht auf Groß und Kleinschreibung geachtet wird. |
| ----------------------------- | ---------------------------------------- |
| /^[\w-.]+@[\w-]+.[a-z]{2,}$/i | **Eine einfache Email-Validierung.**Am Anfang dürfen beliebig viele Buchstaben, Zahlen, Punkte, Unterstriche und Bindestriche stehen.Dann muss ein @ folgen und danach wieder beliebig viele Buchstaben, Zahlen, Unterstriche und Bindestriche.Hiernach wird ein Punkt erwartet und dann 2-4 Groß- oder Kleinbuchstaben für die Topleveldomain. |

## Das Helferlein

Natürlich gibt es auch ein paar Helferlein. Da ich aber meine Regular Expressions meistens mit Trial and Error validiere, habe ich nur ein Tool, dass ich hierfür gerne mal zur Hilfe ziehe.
Das Tool hat eine Eingabe für die RegEx und ein Textfeld, in dem der Text steht, der validiert werden soll. Hier sieht man wegen der verschiedenen Farbhervorhebungen immer sehr schnell wo noch etwas geändert werden muss.

Zu finden ist dieses Tool unter: http://www.regexe.de/