class GiroKonto extends Konto {
    constructor(ktoNr, ktoInhaber, ktoSaldo, dispo) {
        super(ktoNr, ktoInhaber, ktoSaldo)
        this.dispo = dispo
        this.type = 'giro'
    }
      abheben (betrag) {
          if (this.ktoSaldo - betrag >= - this.dispo ) {
                super.abheben(betrag)
                return true
            } else {
                return false
            }
      } 
      einzahlen(betrag) {
          super.einzahlen(betrag)
      }
      toString() {
        return super.toString() + `<br> Dispositionskredit: ${this.dispo.toString()}`
    }
    ueberweisen(betrag, zielKonto) {
        if (this.abheben(betrag) ) {
          zielKonto.einzahlen(betrag)
          return true
        } else {
            return false
        }
    }
}