let konto
let kontos = new KontenVerwaltung()
initKontos()
console.log(kontos.toString())

function initKontos() {
    konto = new GiroKonto("47111020", "Mueller", 2500, 1000)
    kontos.addKonto(konto)
    konto = new GiroKonto("47111050", "Keller", 4320, 3000)
    kontos.addKonto(konto)
}
function kontoErzeugen() {
    try{
        document.getElementById("ausgabe").innerHTML = ``
        const wahl = document.getElementById("girokonto").checked
        const ktoNr = document.getElementById("ktonr").value
        const ktoInh =  document.getElementById("ktoInh").value
        const saldo = parseFloat(document.getElementById("saldo").value)
        if ( wahl ) {
            const dispo = 2000  // Dies kann später auch als Eingabefeld ergänzt werden
            konto = new GiroKonto(ktoNr, ktoInh, saldo, dispo)
            //
            let text = kontos.addKonto(konto)
            document.getElementById("ausgabe").innerHTML = konto+'<br>'+text
        }
        else{
            const zinsSatz = 0.1 //  Dies kann später auch als Eingabefeld ergänzt werden
         
            konto = new SparKonto(ktoNr, ktoInh, saldo, zinsSatz)
           let text = kontos.addKonto(konto)
           document.getElementById("ausgabe").innerHTML = konto+'<br>'+text
        }
    }    
    catch(error){
        alert(error.message);
    }
}

function alleKontenAnzeigen() {
    
    document.getElementById("ausgabe").innerHTML = kontos.toString()
}

function ausgeben(kto) {
    document.getElementById("ausgabe").innerHTML = kto.toString()
}

function betragAuslesen() {
    return parseFloat(document.getElementById("betrag").value)
}
function abheben () {
    const betrag = betragAuslesen()
    const bKtoNr = document.getElementById("bktonr").value
    konto = kontos.findKonto(bKtoNr)
    if(konto !== undefined) {
    const istOk = konto.abheben(betrag)  
    if (istOk) {
        ausgeben(konto)
        } else {
            document.getElementById("ausgabe").innerHTML = 'Abhebung nicht möglich'
        }
    } else { 
        document.getElementById("ausgabe").innerHTML = 'Kontonummer existiert nicht'
    }
}
function einzahlen() {
    const betrag = betragAuslesen()
    const bKtoNr = document.getElementById("bktonr").value
    konto = kontos.findKonto(bKtoNr)
    if(konto !== undefined) {
        konto.einzahlen(betrag)
        ausgeben(konto)
    } else {
        document.getElementById("ausgabe").innerHTML = 'Kontonummer existiert nicht'
    }
}
 
function ueberweisen() {
    const betrag = betragAuslesen()
    const vonKtoNr = document.getElementById("bktonr").value
    const zielKtoNr = document.getElementById("zielkto").value
    //
    const start = kontos.findKonto(vonKtoNr)
    const ziel = kontos.findKonto(zielKtoNr)
         if(!start.ueberweisen(betrag, ziel)) {
            document.getElementById("ausgabe").innerHTML = 'Überweisung nicht möglich'
        } else {
            alleKontenAnzeigen()
    }
}
function eingabeZahlPruefen(){
   var inpKto = document.getElementById("ktonr")
   var errMessage = document.getElementById("errKontonummer")
    if(isNaN(inpKto.value)){
       //
       inpKto.classList.add("error")
       errMessage.innerHTML="Für eine Kontonummer nur Ziffern eingeben!"
       document.getElementById("btnKontoErzeugen").removeEventListener("click",kontoErzeugen)

       inpKto.focus()
        
    }else{
       inpKto.classList.remove("error")
       errMessage.innerHTML=""
       document.getElementById("btnKontoErzeugen").addEventListener("click",kontoErzeugen)
    }    
}


document.getElementById("btnKontoErzeugen").addEventListener("click",kontoErzeugen)
document.getElementById("btnAuszahlen").addEventListener("click", abheben)
document.getElementById("btnEinzahlen").addEventListener("click", einzahlen)
document.getElementById("btnAlleKontenAnzeigen").addEventListener("click", alleKontenAnzeigen)
document.getElementById("ktonr").addEventListener("blur", eingabeZahlPruefen)
document.getElementById("btnueberweisen").addEventListener("click", ueberweisen)
