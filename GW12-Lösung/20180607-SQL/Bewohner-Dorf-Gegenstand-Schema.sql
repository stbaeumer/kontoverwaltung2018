PRAGMA foreign_keys = 1;

CREATE TABLE bewohner (
  bewohnernr INTEGER,
  name VARCHAR(30),
  dorfnr INTEGER,
  geschlecht VARCHAR(30),
  beruf VARCHAR(30),
  gold INTEGER,
  status  VARCHAR(30),
  PRIMARY KEY(bewohnernr)--,
  --FOREIGN KEY (dorfnr) REFERENCES dorf(dorfnr)
);

CREATE TABLE dorf (
  dorfnr INTEGER,
  name VARCHAR(30),
  haeuptling INTEGER,
  PRIMARY KEY(dorfnr),
  FOREIGN KEY (haeuptling) REFERENCES bewohner(bewohnernr)
);


CREATE TABLE gegenstand (
  gegenstand VARCHAR(30),
  besitzer INTEGER,
  PRIMARY KEY(gegenstand),
  FOREIGN KEY (besitzer) REFERENCES bewohner(bewohnernr)
);


INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (1, 'Paul Backmann', 2, 'm', 'Baecker', 850, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (2, 'Ernst Peng', 3, 'm', 'Waffenschmied', 280, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (3, 'Rita Ochse', 1, 'w', 'Baecker', 350, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (4, 'Carl Ochse', 1, 'm', 'Kaufmann', 250, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (6, 'Gerd Schlachter', 2, 'm', 'Metzger', 4850, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (7, 'Peter Schlachter', 3, 'm', 'Metzger', 3250, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (8, 'Arthur Schneiderpaule', 2, 'm', 'Pilot', 490, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (9, 'Tanja Trommler', 1, 'w', 'Baecker', 550, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (10, 'Peter Trommler', 1, 'm', 'Schmied', 600, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (12, 'Otto Armleuchter', 2, 'm', 'Haendler', 680, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (13, 'Fritz Dichter', 2, 'm', 'Hoerbuchautor', 420, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (14, 'Enrico Zimmermann', 3, 'm', 'Waffenschmied', 510, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (15, 'Helga Rasenkopf', 2, 'w', 'Haendler', 680, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (16, 'Irene Hutmacher', 1, 'w', 'Haendler', 770, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (17, 'Erich Rasenkopf', 3, 'm', 'Metzger', 990, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (18, 'Rudolf Gaul', 3, 'm', 'Hufschmied', 390, 'friedlich');
INSERT INTO bewohner (bewohnernr, name, dorfnr, geschlecht, beruf, gold, status) VALUES (19, 'Anna Flysh', 2, 'w', 'Metzger', 2280, 'friedlich');


INSERT INTO dorf (dorfnr, name,haeuptling) VALUES (1, 'Affenstadt', 1);
INSERT INTO dorf (dorfnr, name,haeuptling) VALUES (2, 'Gurkendorf', 6);
INSERT INTO dorf (dorfnr, name,haeuptling) VALUES (3, 'Zwiebelhausen', 13);


INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Teekanne', 15);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Spazierstock', 5);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Hammer', 2);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Ring', 15);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Kaffeetasse', 20);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Eimer', 20);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Seil', 17);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Pappkarton', 20);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Gluehbirne', 20);
INSERT INTO gegenstand (gegenstand, besitzer) VALUES ('Schwert', 20);
