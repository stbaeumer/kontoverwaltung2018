PRAGMA foreign_keys = 1;

CREATE TABLE Mitarbeiter (
  MitarbeiterId INTEGER,
  MitarbNName VARCHAR(30),
  MitarbVName VARCHAR(30),
   AbteilungId INTEGER,  
  PRIMARY KEY(MitarbeiterId),
  FOREIGN KEY (AbteilungId) REFERENCES Abteilung(AbteilungId)
);

CREATE TABLE Abteilung (
  AbteilungId INTEGER,
  Bezeichnung VARCHAR(30),
  PRIMARY KEY(AbteilungId)
);

CREATE TABLE Projekt (
  ProjektId INTEGER,
  Projektbeschreibung VARCHAR(30),
  PRIMARY KEY(ProjektId)
);

CREATE TABLE MitarbeiterProjekt (
  MitarbeiterId INTEGER,
  ProjektId INTEGER,
  Arbeitsstunden INTEGER,
  PRIMARY KEY(MitarbeiterId,ProjektId),
  FOREIGN KEY (MitarbeiterId) REFERENCES Mitarbeiter(MitarbeiterId),
  FOREIGN KEY (ProjektId) REFERENCES Projekt(ProjektId)
);

INSERT INTO Abteilung (AbteilungId, Bezeichnung) VALUES (1, 'Einkauf');
INSERT INTO Abteilung (AbteilungId, Bezeichnung) VALUES (2, 'Controlling');
INSERT INTO Abteilung (AbteilungId, Bezeichnung) VALUES (3, 'Verkauf');

INSERT INTO Projekt (ProjektId, Projektbeschreibung) VALUES (1, 'Ein Projekt');
INSERT INTO Mitarbeiter (MitarbeiterId, MitarbNName, MitarbVName, AbteilungId) VALUES (1, 'Müller', 'Egon', 1);
INSERT INTO Mitarbeiter (MitarbeiterId, MitarbNName, MitarbVName, AbteilungId) VALUES (2, 'Meier', 'Jutta', 1);

INSERT INTO MitarbeiterProjekt (MitarbeiterId, ProjektId, Arbeitsstunden) VALUES (1,1, 10);

-- FOREIGN KEY constraint failed:

DELETE FROM Abteilung WHERE AbteilungId = 1;


INSERT INTO MitarbeiterProjekt (MitarbeiterId, ProjektId, Arbeitsstunden) VALUES (7,1, 10);

