### SQL ISLAND

![1528351891763](C:\Users\gkohl\AppData\Local\Temp\1528351891763.png)

Dorf(**dorfnr**, name, haeuptling)

Bewohner(**bewohnernr**, name, dorfnr, geschlecht, beruf, gold, status)

Gegenstand(**gegenstand**, besitzer)

1. Und jede Menge Bewohner gibt es hier auch. Zeige mir die Liste der Bewohner.

   ```SELECT * FROM BEWOHNER```

   Beispiel: Alle Bewohner, die Metzger sind:

   ```SELECT * FROM BEWOHNER WHERE BEruf = 'Metzger' ```

   

2. Welche BEwohner sind friedlich?

   ```sql
   SELECT * FROM BEWOHNER
   WHERE STATUS = 'friedlich'
   ```

   

3. Welcher BEwohner sind friedlich und gehören zu den Waffenschmieden?

   ```sql
   SELECT * FROM BEWOHNER 
   WHERE Status = 'friedlich' AND beruf='Waffenschmied'
   ```

   

4. Noch andere Schmiede?

   ```sql
   SELECT * FROM BEWOHNER 
   WHERE Status = 'friedlich' AND beruf LIKE '%schmied'
   ```

5. Welche Bewohnernr hat "Fremder'?

   ```sql
   SELECT Bewohnernr 
   FROM BEWOHNER 
   WHERE name = 'Fremder'
   ```

   Antwort: Nr. 20.

6. Wie viel Gold hat "Fremder'?

   ```sql
   SELECT gold 
   FROM BEWOHNER 
   WHERE name = 'Fremder'
   ```

   Antwort: 0

7. Welche Gegenstände sind ohne Besitzer vorhanden?

   ```sql
   SELECT gegenstand, besitzer 
   FROM Gegenstand 
   WHERE besitzer IS NULL
   ```

   ![1528353547933](C:\Users\gkohl\AppData\Local\Temp\1528353547933.png)

8. Alle Gegenstände ohne Besitzer einsammeln!

   ```sql
   UPDATE gegenstand 
   SET besitzer = 20 
   WHERE besitzer IS NULL
   ```

9. Welche Gegenstände habe ich jetzt?

   ```sql
   SELECT * FROM Gegenstand
   WHERE besitzer = 20
   ```

10. Finde friedliche Bewohner mit dem Beruf Haendler oder Kaufmann. Eventuell möchten sie etwas von uns kaufen. (Hinweis: Achte bei AND- und OR-Verknüpfungen auf korrekte Klammerung)

    ![1528354085311](C:\Users\gkohl\AppData\Local\Temp\1528354085311.png)

    

11. Beweohner nr. 15 möchte Ring und Teekanne

    ```sql
    UPDATE GEGENSTAND
    SET BEsitzer = 15 
    WHERE Gegenstand = 'Ring' OR Gegenstand = 'Teekanne' AND Besitzer = 20
    ```

    

12. 'Fremder' umtaufen

    ```sql
    UPDATE BEWOHNER
    SET name = 'NeuHier' 
    WHERE bewohnernr = 20
    ```

    

13. Alle Bäcker absteigend sortiert nach Vermögen!

    ```sql
    SELECT * FROM BEwohner 
    WHERE beruf = 'Bäcker'
    ORDER BY gold DESC
    ```

    

14. 100000 Brötchen verkauft und Schwert gekauft!

    ![1528354745571](C:\Users\gkohl\AppData\Local\Temp\1528354745571.png)

    

15. Pilotensuche

    ```sql
    SELECT * FROM BEwohner 
    WHERE beruf = 'Pilot'
    ```

    

16. Tabellen verknüpfen, noch alter SQL-Standard!!!

    ```
    SELECT dorf.name FROM dorf, bewohner 
    WHERE dorf.dorfnr = bewohner.dorfnr AND bewohner.name = 'Dirty Dieter'
    ```

    

17. Häuptling von Zwiebelhausen

    ```
    SELECT DISTINCT bewohner.name FROM 
    BEwohner INNER JOIN DORF
    ON Bewohner.dorfnr = Dorf.dorfnr
    WHERE dorf.name = 'Zwiebelhausen' AND BEWOHNER.bewohnernr=dorf.Haeuptling
    ```

    

18. Bewohner in Zwieblehausen

    ```sql
    SELECT COUNT(*) FROM bewohner, dorf 
    WHERE dorf.dorfnr = bewohner.dorfnr AND dorf.name = 'Zwiebelhausen' 
    ```

    

19. Wie viele Frauen in Zwieblehausen?

    ```sql
    SELECT COUNT(*) FROM bewohner INNER JOIN dorf 
    ON dorf.dorfnr = bewohner.dorfnr  
    WHERE dorf.name = 'Zwiebelhausen' AND 
    Geschlecht = 'w'
    ```

20. Name dieser Frau?

    ```sql
    SELECT bewohner.Name FROM bewohner INNER JOIN dorf 
    ON dorf.dorfnr = bewohner.dorfnr  WHERE dorf.name = 'Zwiebelhausen' AND 
    Geschlecht = 'w'
    ```

    Antwort: Dirty Doerthe

21. Wieviel Gold haben alle Haendler, Kaufleute und Bäcker zusammen?

    ```
    SELECT SUM(Gold)
    FROM BEwohner
    WHERE beruf IN ('Haendler', 'Bäcker', 'Kaufmann')
    ```

    

22. Durchschnittliches Goldvermögen nach Status

    ```sql
    SELECT Status, AVG(Gold)
    FROM BEwohner
    GROUP BY Status
    ```

    

23. Auch Doerthe verschwindet von der Insel.

    ```
    DELETE FROM Bewohner WHERE name ='Dirty Doerthe'
    ```

    

24. Pilot befreien

    ```
    UPDATE Bewohner 
    SET Status = 'friedlich'
    WHERE Beruf = 'Pilot'
    ```

    

25. ...

    

    

    