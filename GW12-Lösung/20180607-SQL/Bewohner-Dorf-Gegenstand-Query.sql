Select * from dorf;
Select * from gegenstand;
Select * from bewohner;

-- 1. Alle Bewohner, die Metzger sind

SELECT * 
FROM BEWOHNER 
WHERE Beruf = 'Metzger';

-- 2. Welche BEwohner sind friedlich?

SELECT * 
FROM BEWOHNER 
WHERE STATUS = 'friedlich';

-- 3. Welcher Bewohner sind friedlich und gehören zu den Waﬀenschmieden?

SELECT * 
FROM BEWOHNER 
WHERE STATUS = 'friedlich'  AND beruf='Waffenschmied';

-- 4. Noch andere Schmiede?

SELECT * 
FROM BEWOHNER 
WHERE Status = 'friedlich' AND beruf LIKE '%schmied';

-- 5. Welche Bewohnernr hat "Fremder'?

SELECT Bewohnernr 
FROM BEWOHNER 
WHERE name = 'Fremder';

-- 6. Wie viel Gold hat "Fremder'?

SELECT gold 
FROM BEWOHNER 
WHERE name = 'Fremder';

-- 7. Welche Gegenstände sind ohne Besitzer vorhanden?

SELECT gegenstand, besitzer 
FROM Gegenstand 
WHERE besitzer IS NULL;

-- 8. Alle Gegenstände ohne Besitzer einsammeln!

SELECT gegenstand, besitzer 
FROM Gegenstand 
WHERE besitzer IS NULL;

-- 9. Welche Gegenstände habe ich jetzt?

SELECT * 
FROM Gegenstand 
WHERE besitzer = 20;

-- 10. Finde friedliche Bewohner mit dem Beruf Haendler oder Kaufmann. Eventuell möchten sie etwas von uns kaufen. (Hinweis: Achte bei AND- und OR-Verknüpfungen auf korrekte Klammerung)

SELECT * 
FROM Bewohner 
WHERE status = 'friedlich' AND (BERUF = 'Haendler' OR BERUF = 'Kaufmann');

-- 11. Bewohner Nr. 15 möchte Ring und Teekanne

UPDATE GEGENSTAND
SET BEsitzer = 15 
WHERE Gegenstand = 'Ring' OR Gegenstand = 'Teekanne' AND Besitzer = 20;

-- 12. 'Fremder' umtaufen

UPDATE BEWOHNER
SET name = 'NeuHier' 
WHERE bewohnernr = 20;

-- 13. Alle Bäcker absteigend sortiert nach Vermögen!

SELECT * FROM BEwohner 
WHERE beruf = 'Baecker'
ORDER BY gold DESC;

-- 14. 100000 Brötchen verkauft und Schwert gekauft!

UPDATE bewohner 
SET GOLD = gold + 100 - 150 
WHERE bewohnernr = 20;

--INSERT INTO gegenstand(gegenstand, besitzer) VALUES ('Schwert',20);

-- 15. Pilotensuche

SELECT * FROM Bewohner 
WHERE beruf = 'Pilot';

-- 16. Tabellen verknüpfen, noch alter SQL-Standard!!!

SELECT dorf.name 
FROM dorf, bewohner 
WHERE dorf.dorfnr = bewohner.dorfnr AND bewohner.name = 'Dirty Dieter';

-- 17. Häuptling von Zwiebelhausen

SELECT DISTINCT bewohner.name 
FROM Bewohner INNER JOIN DORF
ON Bewohner.dorfnr = Dorf.dorfnr
WHERE dorf.name = 'Zwiebelhausen' AND BEWOHNER.bewohnernr=dorf.Haeuptling;

-- 18. Bewohner in Zwieblehausen

SELECT COUNT(*) 
FROM bewohner, dorf 
WHERE dorf.dorfnr = bewohner.dorfnr AND dorf.name = 'Zwiebelhausen';

-- 19. Wie viele Frauen in Zwieblehausen?

SELECT COUNT(*) 
FROM bewohner INNER 
JOIN dorf ON dorf.dorfnr = bewohner.dorfnr 
WHERE dorf.name = 'Zwiebelhausen' AND Geschlecht = 'w';

-- 20. Name dieser Frau?

SELECT bewohner.Name 
FROM bewohner INNER 
JOIN dorf ON dorf.dorfnr = bewohner.dorfnr  
WHERE dorf.name = 'Zwiebelhausen' AND geschlecht = 'w';

-- 21. Wieviel Gold haben alle Haendler, Kauﬂeute und Bäcker zusammen?

SELECT SUM(Gold) 
FROM Bewohner 
WHERE beruf IN ('Haendler', 'Bäcker', 'Kaufmann');

-- 22. Durchschnittliches Goldvermögen nach Status

SELECT status, AVG(gold) 
FROM bewohner 
GROUP BY status;

-- 23. Auch Doerthe verschwindet von der Insel.

DELETE 
FROM bewohner 
WHERE name ='Dirty Doerthe';

-- 24. Pilot befreien

--UPDATE Bewohner SET status = 'friedlich' WHERE beruf = 'Pilot';