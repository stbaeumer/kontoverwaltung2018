class Konto{
    constructor(kontonummer, kontoinhaber, kontosaldo) {
        this.kontonummer = kontonummer
        this.kontoinhaber = kontoinhaber
        this.kontosaldo = kontosaldo
    }    

    einzahlen(betrag){
        this.kontosaldo = this.kontosaldo + betrag
    }

    auszahlen(betrag){
        this.kontosaldo = this.kontosaldo - betrag
    }
}