let konto

function kontoErzeugen() {    
    const kontonummer = document.getElementById("tbxKontonummer").value
    const kontoinhaber = document.getElementById("tbxKontoinhaber").value
    const kontosaldo = parseFloat(document.getElementById("tbxKontosaldo").value)

    konto = new Konto(kontonummer,kontoinhaber,kontosaldo)

    ausgeben(konto)
}

function ausgeben(konto) {    
    document.getElementById("ausgabe").innerHTML = konto.kontonummer + "<br>" + konto.kontoinhaber + "<br>" + konto.kontosaldo
}

document.getElementById("btnKontoErzeugen").addEventListener("click", kontoErzeugen)

function einzahlen() {
    const betrag = parseFloat(document.getElementById("tbxBetrag").value)
    konto.einzahlen(betrag)    
    ausgeben(konto)
}

function auszahlen() {
    const betrag =  document.getElementById("tbxBetrag").value

    if(betrag <= konto.kontosaldo){
        konto.auszahlen(betrag)    
        ausgeben(konto)
    }
}

document.getElementById("btnEinzahlen").addEventListener("click", einzahlen)
document.getElementById("btnAuszahlen").addEventListener("click", auszahlen)



