

function fahrradAnlegen() {    
    const artikelnummer = parseFloat(document.getElementById("tbxArtikelnummer").value)
    const artikelname = document.getElementById("tbxArtikelname").value
    const produktgruppe = document.getElementById("tbxProduktgruppe").value    
    const ekPreis = document.getElementById("tbxEkPreis").value
    const vkPreis = document.getElementById("tbxVkPreis").value

    let fahrrad = new Fahrrad(artikelnummer,artikelname,produktgruppe, ekPreis, vkPreis)

    ausgeben(fahrrad)
}

function ausgeben(fahrrad) {    
    document.getElementById("ausgabe").innerHTML = fahrrad.artikelnummer + "<br>" + fahrrad.artikelname + "<br>" + fahrrad.produktgruppe+ "<br>" + fahrrad.ekPreis + "<br>" + fahrrad.vkPreis
}

document.getElementById("btnFahrradAnlegen").addEventListener("click", fahrradAnlegen)



