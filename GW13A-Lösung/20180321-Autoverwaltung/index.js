// Mein Nachname, mein Vorname
'use strict'

let nameDerDatenbank = "meineDatenbank17";

class Auto{
    constructor(leistungInPs, farbe){
        this.aId
        this.leistungInPs = leistungInPs;   
        this.farbe = farbe;
        this.leistungInKw = this.psNachKwUmrechnen();
    }
  
    psNachKwUmrechnen(){
        return 0.735499 * parseFloat(this.leistungInPs);
    }

    ausgabe(){                
        return "aId: " + this.aId + " Farbe: " + this.farbe + " Leistung: " + this.leistungInPs + " PS (" + this.leistungInKw + " kW)";
    }
} // Ende der Klassendefintion

class Person{
    constructor(name, plz){
        this.pId
        this.name = name;   
        this.plz = plz;
    }
  
    ausgabe(){                
        return "pId: " + this.pId + " PLZ: " + this.plz;
    }
} // Ende der Klassendefintion

class Fahrer{
    constructor(aId, pId){
        this.pId
        this.aId
    }
  
    ausgabe(){                
        return "pId: " + this.pId + " aId: " + this.plz;
    }
} // Ende der Klassendefintion

function einAutoAnlegenOnClick() {    
    const leistungInPs = parseFloat(document.getElementById('tbxPs').value);        
    const farbe = document.getElementById('tbxFarbe').value;    
    let auto = new Auto(leistungInPs,farbe);
    console.log(auto);
    console.log("INSERT INTO Autos ...")
    db.run("INSERT INTO Autos(leistungInPs, farbe) VALUES (?,?)", [auto.leistungInPs, auto.farbe]);
    console.log("Nach INSERT, UPDATE oder DELETE muss die Datenbank jetzt auf die Festplatte geschrieben werden ...");    
    window.localStorage.setItem(nameDerDatenbank, datenbankAufFestplatteSchreiben(db.export()));        
    document.getElementById("lblAusgabe").innerHTML = "NEUES AUTO: " + auto.ausgabe();
}

function allesAnzeigenOnClick(){    
    let abfrageErgebnisAutos = db.prepare("SELECT * FROM Autos");    
    let abfrageErgebnisPersonen = db.prepare("SELECT * FROM Personen");    
    let abfrageErgebnisFahrer = db.prepare("SELECT * FROM Fahrer");    
    
    let ausgabeString = "";    
    ausgabeString += tabelleAnzeigen("Alle Autos",abfrageErgebnisAutos);
    ausgabeString += tabelleAnzeigen("Alle Personen",abfrageErgebnisPersonen);
    ausgabeString += tabelleAnzeigen("Alle Fahrer",abfrageErgebnisFahrer);

    document.getElementById("lblAusgabe").innerHTML = ausgabeString;
}

function ausgeben(liste){
    let ausgabestring = "";
    for (var i = 0; i < liste.length; i++) {        
        console.log(liste[i]);
        ausgabestring += liste[i].ausgabe() + "<br>";
    }
    return ausgabestring;
}

function allegrünenAutosAnzeigenOnClick()
{
    let abfrageErgebnis = db.prepare("SELECT * FROM AUTOS Where farbe = 'grün'");    
    let autoListe = [];

    // Das Abfrageergebnis wird zeilenweise durchlaufen.
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();         
        let a = new Auto();
        a.aId = row.aId;       
        a.farbe = row.farbe;
        a.leistungInPs = row.leistungInPs;   
        a.leistungInKw = a.psNachKwUmrechnen();        
        autoListe.push(a)                
    }   
    console.log(autoListe);
    ausgeben(autoListe)
    document.getElementById("lblAusgabe").innerHTML =  tabelleAnzeigen("Alle grünen Autos",abfrageErgebnis)
}

function alleFarbenAnzeigenOnClick()
{
    let abfrageErgebnis = db.prepare("SELECT farbe FROM AUTOS ");    
    let autoListe = [];

    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();         
        let a = new Auto();
        a.aId = row.aId;       
        a.farbe = row.farbe;        
        a.pId = row.pId;   
        autoListe.push(a)                
    }   
    console.log(autoListe);
    ausgeben(autoListe)
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Alle Farben",abfrageErgebnis)
}

function alleVerschiednenFarbenAnzeigenOnClick()
{
    let abfrageErgebnis = db.prepare("SELECT DISTINCT farbe FROM AUTOS");    
    let liste = [];

    // Das Abfrageergebnis wird zeilenweise durchlaufen.
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();         
        let a = new Auto();        
        a.farbe = row.farbe;                
        liste.push(a)                
    }   
    console.log(liste);    
    ausgeben(liste);
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Alle verschiednen Farben",abfrageErgebnis)
}

function alleAutosAnzeigenSortiertNachFarbeAbsteigendOnClick()
{
    let abfrageErgebnis = db.prepare("SELECT * FROM Autos Order by farbe DESC");   //ASC aufsteigend DESC absteigend 
    let autoListe = [];

    // Das Abfrageergebnis wird zeilenweise durchlaufen.
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject();                 
        let a = new Auto();     
        a.farbe = row.farbe;    
        autoListe.push(a)                
    }   
    console.log(autoListe);
    ausgeben(autoListe);
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Autos Anzeigen Sortiert Nach Farbe Absteigend",abfrageErgebnis)
}

// Inner Join (nur solche Autos, die auch gefahren werden)

function autosDieGefahrenWerdenOnClick(){
    let abfrageErgebnis = db.prepare("SELECT Autos.aId, Autos.farbe From Fahrer INNER JOIN Autos on Fahrer.aId = Autos.aId");        
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Autos, die auch gefahren werden",abfrageErgebnis)
}

// Left Join (Alle Autos. Sofern Sie gefahren werden mit Fahrer)

function alleAutosMitUndOhneFahrerOnClick(){
    let abfrageErgebnis = db.prepare("SELECT Autos.aId, Autos.farbe, Fahrer.pId From Autos Left JOIN Fahrer on Fahrer.aId = Autos.aId");        
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Autos, die auch gefahren werden",abfrageErgebnis)
}

// Left Join (Alle Personen. Sofern Sie Fahrer sind, auch das Auto)

function allePersonenMitUndOhneAutoOnClick(){
    let abfrageErgebnis = db.prepare("SELECT Personen.pId, fahrer.aId From Personen LEFT JOIN Fahrer on Fahrer.pId = Personen.pId");        
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Autos, die auch gefahren werden",abfrageErgebnis)
}

// Update

function alleRotenAutosBlauMachenOnClick(){
    db.run("UPDATE Autos SET farbe = 'blau' WHERE farbe = 'rot'");
    window.localStorage.setItem(nameDerDatenbank, datenbankAufFestplatteSchreiben(db.export()));        
    allesAnzeigenOnClick()
}

// Delete

function alleBlauenAutosLöschenOnClick(){
    db.run("DELETE FROM Autos WHERE farbe = 'blau'");
    window.localStorage.setItem(nameDerDatenbank, datenbankAufFestplatteSchreiben(db.export()));        
    allesAnzeigenOnClick()
}

// Count

function autosNachFarbenZählenOnClick(){    
    let abfrageErgebnis = db.prepare("SELECT farbe, COUNT(*) FROM Autos GROUP BY farbe");        
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Anzahl der Autos je Farbe",abfrageErgebnis)
}

// Count mit Bedingung (WHERE funktioniert bei Aggregatfunktionen (COUNT, MAX, MIN) nicht)

function autosNachFarbenZählenSofernMehrAls2OnClick(){    
    let abfrageErgebnis = db.prepare("SELECT farbe, COUNT(aId) FROM Autos GROUP BY farbe HAVING COUNT(aId) > 2");        
    document.getElementById("lblAusgabe").innerHTML = tabelleAnzeigen("Anzahl der Autos je Farbe",abfrageErgebnis)
}

function tabelleAnzeigen(titel, abfrageErgebnis){
    let ausgabestring = titel +"<br>"
    let kopfzeile = ""
    while(abfrageErgebnis.step()) {
        var row = abfrageErgebnis.getAsObject()             
        ausgabestring += "<tr>"
        kopfzeile = "<tr>"
        Object.keys(row).forEach(function(column) {
            ausgabestring += "<td>"                
            ausgabestring += row[column]
            ausgabestring += "</td>"
            kopfzeile += "<td>"                
            kopfzeile += column
            kopfzeile += "</td>"
        });
        ausgabestring += "</tr>"
        kopfzeile += "</tr>"
    }   

    ausgabestring = "<table border='6'>" + kopfzeile + ausgabestring + "</table>"
    return ausgabestring;
}








// Ab hier kommt die Datenbankfunktionalität.

let sql = require('./sql.js'); // Das SQLite-Datenbank-Modul wird eingebunden.

let db;

if(localStorage.getItem(nameDerDatenbank) == null){
    console.log("Es existiert noch keine Datenbank. Sie wird jetzt erstellt ...");
    db = new sql.Database();
}
else{
    console.log("Die existierende Datenbank wird geöffnet ...")     
    db = new sql.Database(datenbankVonFestplatteLaden(localStorage.getItem(nameDerDatenbank)));    
}

// Eine Tabelle zur Aufnahme der von Daten wird angelegt.
db.run("CREATE TABLE IF NOT EXISTS Autos (aId INTEGER PRIMARY Key AUTOINCREMENT, leistungInPs int, farbe char);");
db.run("CREATE TABLE IF NOT EXISTS Personen (pId INTEGER PRIMARY Key AUTOINCREMENT, plz char, Name char);");
db.run("CREATE TABLE IF NOT EXISTS Fahrer (aId INTEGER, pId INTEGER, PRIMARY Key (aId, pId));");
db.run("INSERT OR IGNORE INTO Personen(plz, Name) VALUES (?,?)", ["46325", "Müller"]);
db.run("INSERT OR IGNORE INTO Personen(plz, Name) VALUES (?,?)", ["48653", "Meyer"]);
db.run("INSERT OR IGNORE INTO Autos(leistungInPs, farbe) VALUES (?,?)", [45, "grün"]);
db.run("INSERT OR IGNORE INTO Autos(leistungInPs, farbe) VALUES (?,?)", [90, "rot"]);
db.run("INSERT OR IGNORE INTO Autos(leistungInPs, farbe) VALUES (?,?)", [110, "blau"]);
db.run("INSERT OR IGNORE INTO Fahrer(aId, pId) VALUES (?,?)", [1, 1]);



// Diese Funktion schreibt die Datenbank als Datei auf die Festplatte
function datenbankAufFestplatteSchreiben (arr) {
    var uarr = new Uint8Array(arr);
    var strings = [], chunksize = 0xffff;
    // There is a maximum stack size. We cannot call String.fromCharCode with as many arguments as we want
    for (var i=0; i*chunksize < uarr.length; i++){
        strings.push(String.fromCharCode.apply(null, uarr.subarray(i*chunksize, (i+1)*chunksize)));
    }    
    return strings.join('');
}

// Diese Funktion lädt die Datenbank von der Festplatte in den Arbeitsspeicher
function datenbankVonFestplatteLaden (str) {
    var l = str.length, arr = new Uint8Array(l);
    for (var i=0; i<l; i++) arr[i] = str.charCodeAt(i);    
    return arr;
}

/* 
Referenzen:
===========
https://github.com/kripken/sql.js/wiki/Persisting-a-Modified-Database
https://bitbucket.org/stbaeumer/javascript
http://www.sql-und-xml.de/sql-tutorial/
*/